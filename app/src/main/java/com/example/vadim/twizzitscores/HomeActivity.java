package com.example.vadim.twizzitscores;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.MaterialMenuView;
import com.example.vadim.twizzitscores.favteams.FavoritesHelper;
import com.example.vadim.twizzitscores.favteams.FragmentPrefTeams;
import com.example.vadim.twizzitscores.login.LoginActivity;
import com.example.vadim.twizzitscores.navdrawer.NavDrawerAdapter;
import com.example.vadim.twizzitscores.navdrawer.NavDrawerListItem;
import com.example.vadim.twizzitscores.search.SearchActivity;
import com.example.vadim.twizzitscores.teampages.FragmentTeam;
import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.example.vadim.twizzitscores.utilities.Helper;
import com.facebook.Session;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends FragmentActivity {

    //TODO: switch teamid in prefblocks with team title
    //TODO: create screen for when clicking on game
    //TODO: make live game screen
    //TODO: make ranking page with format
    //TODO: change logo
    //TODO: make different languages
    //TODO: check if it works on previous versions
    //TODO: scroll away live game

    public static int displHeight;
    public static int displWidth;
    public static boolean isTeamViewPagerOpen = false;
    private TextView mSearchText;
    private DrawerLayout mDrawerLayout;
    private ImageView mLogoView;
    private RelativeLayout leftRL;
    private FrameLayout frameL;
    private CircleImageView mIProfileD;
    private ListView mDrawerList;
    private String[] mDrawerItems;
    private MaterialMenuView mMatMenu;
    private LinearLayout mLLSearch;
    private ImageView mSearchIcon;
    private View.OnClickListener searchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mMatMenu.animateState(MaterialMenuDrawable.IconState.ARROW);
            final Intent searchIntent = new Intent(HomeActivity.this, SearchActivity.class);
            int[] screenLoc = new int[2];
            mLLSearch.getLocationOnScreen(screenLoc);
            int orientation = getResources().getConfiguration().orientation;
            searchIntent.
                    putExtra("left", screenLoc[0]).
                    putExtra("top", screenLoc[1]).
                    putExtra("height", mLLSearch.getHeight()).
                    putExtra("orientation", orientation);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                        startActivity(searchIntent);
                        overridePendingTransition(0, 0);
                    } catch (Exception e) {
                        e.getLocalizedMessage();
                    }
                }
            }).start();
        }
    };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        mLLSearch = (LinearLayout) findViewById(R.id.llSearch);
        displHeight = this.getResources().getDisplayMetrics().heightPixels;
        displWidth = this.getResources().getDisplayMetrics().widthPixels;

        // change status bar color in Lollipop
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getResources().getColor(R.color.twizzit_blue_dark));

        // methods that create the home activity
        createDrawer(savedInstanceState);
        createSearch(savedInstanceState);
        createFavorites(savedInstanceState);

        // in case activity called from Search Activity
        Intent intent = getIntent();
        String teamId = intent.getStringExtra(SearchActivity.INTENT_TEAMID);
        if (teamId != null) {
            FragmentTeam fragTeam = FragmentTeam.getInstance(this, teamId);
            getSupportFragmentManager().beginTransaction().replace(R.id.containerTeam, fragTeam, teamId).commit();
            isTeamViewPagerOpen = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // reset drawable menu icon to burger
        mMatMenu.setState(MaterialMenuDrawable.IconState.BURGER);
        // register the eventbus handling the server events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister the eventbus handling the server events
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(DownloadEvents se) {
        switch (se.message) {
            case DownloadEvents.START_DOWN_PROFILEPIC:
                break;
            case DownloadEvents.END_DOWN_PROFILEPIC:
                mIProfileD.setImageBitmap(LoginActivity.user.getProfilePic());
                break;
            case DownloadEvents.ERROR_DOWN_PROFILEPIC:
                Toast.makeText(this, "error downloading profile picture", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * create the Favorites frame
     */
    public void createFavorites(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FavoritesHelper.getInstance(this).createFavorites();
            FragmentPrefTeams fragPT = new FragmentPrefTeams();
            getSupportFragmentManager().beginTransaction().replace(R.id.containerTeam, fragPT).commit();
        }
    }

    /**
     * create the search frame *
     */
    public void createSearch(Bundle savedInstanceState) {

        mMatMenu = (MaterialMenuView) findViewById(R.id.material_menu_button);
        mMatMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTheDrawer();
            }
        });
        mLogoView = (ImageView) findViewById(R.id.logoView);
        mLogoView.setOnClickListener(searchListener);
        mSearchIcon = (ImageView) findViewById(R.id.searchIcon);
        mSearchIcon.setOnClickListener(searchListener);
        mSearchText = (TextView) findViewById(R.id.searchText);
        mSearchText.setTypeface(Helper.getTypeFace(this, false));
        mSearchText.setOnClickListener(searchListener);
    }

    /**
     * method to handle back presses *
     */
    public void onBackPressed() {
        Log.d("onbackpressed", "teamviewpageropen: " + isTeamViewPagerOpen);
        if (isTeamViewPagerOpen) {
            createFavorites(null);
            isTeamViewPagerOpen = false;
        } else {
            Log.d("onbackpressed", "yet i come here");
            finish();
        }
    }

    /**
     * create the drawer method
     */
    private void createDrawer(Bundle savedInstanceState) {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        leftRL = (RelativeLayout) findViewById(R.id.whatYouWantInLeftDrawer);
        frameL = (FrameLayout) findViewById(R.id.profileFrame);
        frameL.setBackgroundResource(R.drawable.bg_fade_3);
        mIProfileD = (CircleImageView) findViewById(R.id.iProfileId);
        mIProfileD.setBorderWidth(6);
        mIProfileD.setPadding(2, 2, 2, 2);
        mIProfileD.setBorderColor(getResources().getColor(
                android.R.color.white));
        if (LoginActivity.user.getProfilePic() != null) {
            mIProfileD.setImageBitmap(LoginActivity.user.getProfilePic());
        }
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        NavDrawerAdapter adapter = new NavDrawerAdapter(this, generateData());
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 3:
                        Session session = Session.getActiveSession();
                        session.closeAndClearTokenInformation();
                        Intent intent4 = new Intent(HomeActivity.this,
                                LoginActivity.class);
                        intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent4);
                        break;
                    default:
                        break;
                }
                mDrawerLayout.closeDrawers();
            }
        });

    }

    /**
     * puts menu in drawer
     */
    private ArrayList<NavDrawerListItem> generateData() {
        mDrawerItems = getResources().getStringArray(R.array.drawer_titles);
        int[] icons = new int[]{
                R.drawable.abc_ic_search_api_mtrl_alpha,
                R.drawable.abc_ic_menu_cut_mtrl_alpha,
                R.drawable.abc_ic_menu_paste_mtrl_am_alpha,
                R.drawable.abc_ic_menu_share_mtrl_alpha,
                R.drawable.abc_ic_voice_search_api_mtrl_alpha};
        ArrayList<NavDrawerListItem> models = new ArrayList<>();
        for (int i = 0; i < mDrawerItems.length; i++) {
            models.add(new NavDrawerListItem(icons[i], mDrawerItems[i]));
        }
        return models;
    }

    /**
     * public method to open the drawer by UI
     */
    public void openTheDrawer() {
        mDrawerLayout.openDrawer(leftRL);
    }
}
