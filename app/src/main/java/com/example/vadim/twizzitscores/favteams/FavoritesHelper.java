package com.example.vadim.twizzitscores.favteams;

import android.content.Context;
import android.content.Intent;

import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.example.vadim.twizzitscores.utilities.TeamService;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class FavoritesHelper {

    public static final String PREF_TEAMS = "pref_teams";
    public static ArrayList<String> pref_team_list;
    private static FavoritesHelper instance;
    Context mContext;
    private TinyDB tinydb;

    private FavoritesHelper(Context context) {
        this.mContext = context;
    }

    public static FavoritesHelper getInstance(Context context) {

        if (instance == null) {
            instance = new FavoritesHelper(context);
        }

        return instance;
    }

    public void createFavorites() {
        // make sure we don't load prefs two times

        tinydb = new TinyDB(mContext);
        if (tinydb.getList(PREF_TEAMS) != null) {
            pref_team_list = tinydb.getList(PREF_TEAMS);
        } else {
            pref_team_list = new ArrayList<>();
        }

        // download all info of teams
        for (String prefTeam : pref_team_list) {
            Intent teamIntent = new Intent(mContext, TeamService.class);
            teamIntent.putExtra("team_id", prefTeam);
            mContext.startService(teamIntent);
        }
    }

    public void saveFavorite(String team_id) {
        if (!pref_team_list.contains(team_id)) {
            pref_team_list.add(team_id);
            tinydb.putList(PREF_TEAMS, pref_team_list);
            EventBus.getDefault().post(new DownloadEvents(DownloadEvents.SHAREDPREFS_CHANGED));
        }
    }

    public void removeFavorite(String team_id) {
        if (pref_team_list.contains(team_id)) {
            pref_team_list.remove(team_id);
            tinydb.putList(PREF_TEAMS, pref_team_list);
            EventBus.getDefault().post(new DownloadEvents(DownloadEvents.SHAREDPREFS_CHANGED));
        }
    }

    public boolean isFavorite(String team_id) {
        if (pref_team_list.contains(team_id)) {
            return true;
        } else {
            return false;
        }
    }
}
