package com.example.vadim.twizzitscores.favteams;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.vadim.twizzitscores.HomeActivity;
import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.search.SearchActivity;
import com.example.vadim.twizzitscores.teampages.FragmentTeam;
import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.example.vadim.twizzitscores.utilities.Helper;
import com.example.vadim.twizzitscores.utilities.TeamDataCache;

import java.util.HashMap;

import de.greenrobot.event.EventBus;

public class FragmentPrefTeams extends Fragment {

    private final static String CONT_HEIGHT = "cont_height";
    private final static String TEAM_MAP = "team_map";

    private static HashMap<String, String> teamMap = null;
    private GridLayout grid;
    private View revealView;
    private ViewGroup mcont;
    private int containerHeight;
    private FrameLayout frameLive;
    private FrameLayout frameSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(
                R.layout.fragment_favorites, container, false);
        mcont = container;
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        grid = (GridLayout) getView().findViewById(R.id.gridLay);
        frameLive = (FrameLayout) getView().findViewById(R.id.frameLive);
        revealView = (View) getView().findViewById(R.id.revealView);
        frameSearch = (FrameLayout) getActivity().findViewById(R.id.containerSearch);

        if (savedInstanceState != null) {
            if (savedInstanceState.getInt(CONT_HEIGHT) > 0) {
                containerHeight = savedInstanceState.getInt(CONT_HEIGHT);
            }
            if (savedInstanceState.getSerializable(TEAM_MAP) != null) {
                teamMap = (HashMap<String, String>) savedInstanceState.getSerializable(TEAM_MAP);
            }
            createGrid();
        } else {
            getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int difference = HomeActivity.displHeight - frameLive.getHeight() - frameSearch.getHeight();
                    containerHeight = difference;
                    if (containerHeight > 0) {
                        Log.d("JDLKMFJMDSLJFLSKDJFMLD", "OGL: g,hv,rv :" + mcont.getHeight() + ", " + frameLive.getHeight() + ", " + grid.getHeight()+ ", " + frameSearch.getHeight());
                        Log.d("KFJMLSDJFLMKDJMKLFJD", "OGL: difference : " + difference + ", of: " + HomeActivity.displHeight);
                        // if greater than 0, layout should be done.
                        getView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        createGrid();
                    }
                }
            });
        }
        Log.d("container", "view h,h: " + containerHeight + ", " + getView().getHeight());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CONT_HEIGHT, containerHeight);
        outState.putSerializable(TEAM_MAP, teamMap);
    }

    private void hideRevealView() {

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showRevealView(String teamId, int cx, int cy) {

        // get the final radius for the clipping circle
        int finalRadius = Math.max(revealView.getWidth(), revealView.getHeight());

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(revealView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        revealView.setVisibility(View.VISIBLE);
        revealView.setBackgroundColor(Helper.getColorFromTeamID(teamId));
        anim.setDuration(500).start();
    }

    @SuppressLint("NewApi")
    private void createGrid() {
        grid.removeAllViews();
        int numberOfPrefTeams = FavoritesHelper.pref_team_list.size();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        int pxHome = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
        int columns = ((numberOfPrefTeams <= 4) ? 2 : 3);
        int width = ((HomeActivity.displWidth - (2 * px)) / columns) - (columns * px);
        int height = (containerHeight / columns) - (columns * px) - pxHome;
        grid.setColumnCount(columns);

        // we want to add a "add team" button, that's why <= numberOfPrefTeams
        // if i == numberofprefteams means we're in last iteration, thus add a fav button
        // be smaller than 9 teams
        for (int i = 0; i <= numberOfPrefTeams && i < 9; i++) {

            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            params.setMargins(px, px, px, px);
            params.setGravity(Gravity.CENTER_VERTICAL);
            Button button = new Button(getActivity());
            button.setWidth(width);
            button.setHeight(height);
            button.setLayoutParams(params);
            button.setTypeface(Helper.getTypeFace(getActivity(), false));
            button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            button.setTextColor(Color.WHITE);

            if (i == numberOfPrefTeams) {

                button.setBackgroundResource(R.drawable.pref_add_button);

                String strText = "add +";
                button.setText(strText.toLowerCase());

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent searchIntent = new Intent(getActivity(), SearchActivity.class);
                        startActivity(searchIntent);
                    }
                });
            } else {
                final String teamId = FavoritesHelper.pref_team_list.get(i);

                button.setBackgroundResource(R.drawable.pref_team_button);
                button.getBackground().setColorFilter(Helper.getColorFromTeamID(teamId), PorterDuff.Mode.SRC_ATOP);

                if (teamMap != null) {
                    String prefText = Helper.getKeyFromValue(teamMap, teamId).toString();
                    button.setText(prefText);
                } else {
                    button.setText(teamId);
                }
                button.setTag(teamId);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int cx = (v.getLeft() + v.getRight()) / 2;
                        int cy = (v.getTop() + v.getBottom()) / 2;
                        showRevealView(teamId, cx, cy);
                        final String viewTag = v.getTag().toString();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(500);
                                    FragmentTeam fragTeam = FragmentTeam.getInstance(getActivity(), viewTag);
                                    Log.d("FPT: clicked a pref btn", viewTag);
                                    Log.d("FPT: cached teams", TeamDataCache.getInstance().getLru().toString());
                                    Log.d("FPT: fragteam", "fragTeam = " + fragTeam.toString());
                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerTeam, fragTeam, viewTag).commit();
                                    HomeActivity.isTeamViewPagerOpen = true;
                                } catch (Exception e) {
                                    e.getLocalizedMessage();
                                }
                            }
                        }).start();


                    }
                });
                button.setElevation(6f);
            }
            grid.addView(button);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(DownloadEvents.DownloadTeamMap dtm) {
        switch (dtm.message) {
            case DownloadEvents.END_DOWN_TEAMSLIST:
                teamMap = dtm.teamMap;
                createGrid();
                break;
            case DownloadEvents.ERROR_DOWN_TEAMSLIST:
                //TODO: handle error if teamService fails.
                break;
        }
    }

    public void onEventMainThread(DownloadEvents se) {
        switch (se.message) {
            case DownloadEvents.SHAREDPREFS_CHANGED:
                createGrid();
                break;
            default:
                break;
        }
    }
}
