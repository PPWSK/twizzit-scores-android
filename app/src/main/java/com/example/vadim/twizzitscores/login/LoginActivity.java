package com.example.vadim.twizzitscores.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.example.vadim.twizzitscores.HomeActivity;
import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.search.SearchService;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class LoginActivity extends FragmentActivity {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    private final static String TEAMS_URI = "http://develop.twizzit.com/webservice.php?page=rankingsandresults&key=amo85g3e46z&lg=2&action=get_rr_groups";

    // FACEBOOK INSTANCE VARS
    public static GraphUser FBUser;
    public static User user;
    public ProgressDialog dialog;
    private UiLifecycleHelper uiHelper;
    private LoginButton loginButton;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            if (state.isOpened()) {
                startMainScreen();
            }
        }
    };
    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall,
                            Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall,
                               Bundle data) {
            Log.d("HelloFacebook", "Success!");

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new ProgressDialog(this);

        /***
         * 1. Check if Internet
         *************************/

        if (!isOnline()) {

            dialog.setTitle("No internet connection...");
            dialog.setMessage("Closing Application");
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2000);
                        finish();
                    } catch (Exception e) {
                        e.getLocalizedMessage();
                    }
                }
            }).start();

        } else {

            /***
             * 2. Login to Facebook & return GraphUser
             *********************************************/

            uiHelper = new UiLifecycleHelper(this, callback);
            uiHelper.onCreate(savedInstanceState);

            setContentView(R.layout.activity_splash);

            loginButton = (LoginButton) findViewById(R.id.login_button);


            // set the required facebook permissions here:
            loginButton.setReadPermissions(Arrays.asList("user_birthday", "user_likes",
                    "user_friends", "user_hometown", "user_photos",
                    "user_relationships", "user_groups"));
            loginButton
                    .setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
                        @SuppressWarnings("static-access")
                        @Override
                        public void onUserInfoFetched(GraphUser user) {
                            LoginActivity.this.FBUser = user;

                            startMainScreen();
                        }

                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
        AppEventsLogger.activateApp(this);
        startMainScreen();
    }

    private void startMainScreen() {
        String LOG_TAG = "startMainScreen Method";

        Session session = Session.getActiveSession();
        if (session != null && session.isOpened() && FBUser != null) {

            user = new User(FBUser.getId(), FBUser.getFirstName(), FBUser.getLastName(), FBUser.getBirthday());

            // call service for downloading and setting profile-pic
            Intent serviceIntent = new Intent(LoginActivity.this, ProfilePicService.class);
            serviceIntent.putExtra("uri", "https://graph.facebook.com/" + user.getId()
                    + "/picture?type=large");
            startService(serviceIntent);

            // call service for downloading team data
            Intent serviceIntent2 = new Intent(LoginActivity.this, SearchService.class);
            serviceIntent2.putExtra("uri", TEAMS_URI);
            this.startService(serviceIntent2);

            // call activity home
            Intent intent = new Intent(LoginActivity.this,
                    HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

            session.close();

        } else {
            Log.d(LOG_TAG, "session is " + (session == null ? "null" : "not null"));
            Log.d(LOG_TAG, "session.isopened = " + session.isOpened());
            Log.d(LOG_TAG, "the_user is " + (FBUser == null ? "null" : "not null"));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiHelper.onDestroy();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    protected boolean isOnline() {
        getApplicationContext();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            if (netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                Toast.makeText(this, "wifi connection", Toast.LENGTH_SHORT)
                        .show();
            } else if (netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                Toast.makeText(this, "mobile connection", Toast.LENGTH_SHORT)
                        .show();
            }
            return true;
        } else {
            return false;
        }
    }

    @SuppressWarnings("unused")
    private void getKeyHashForFacebook() {
        // method used for creating Facebook KeyHash from machine where you're
        // coding, only necessary if coding and testing from new computer.
        try {
            PackageInfo info = getApplicationContext().getPackageManager()
                    .getPackageInfo("com.example.vadim.twizzitscores",
                            PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("keyhash:", "namenotfound");
        } catch (NoSuchAlgorithmException e) {
            Log.d("keyhash:", "nosuchalgorithm");
        }

    }

}
