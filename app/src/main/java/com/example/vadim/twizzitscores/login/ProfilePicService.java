package com.example.vadim.twizzitscores.login;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import de.greenrobot.event.EventBus;

public class ProfilePicService extends IntentService {

    //private static final String LOG_TAG = ProfilePicService.class.getSimpleName();

    private InputStream inputStream;
    private Bitmap image;
    private OkHttpClient okClient;

    public ProfilePicService() {
        super(null);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        EventBus.getDefault().post(new DownloadEvents(DownloadEvents.START_DOWN_PROFILEPIC));
        okClient = new OkHttpClient();
        URL url;

        try {
            url = new URL(intent.getStringExtra("uri"));
            Log.d("handleintent", "url: " + url);
            Request req = new Request.Builder()
                    .url(url)
                    .build();

            Response resp = okClient.newCall(req).execute();
            inputStream = resp.body().byteStream();
            if ((image = BitmapFactory.decodeStream(new FlushedInputStream(
                    inputStream))) != null) {
                LoginActivity.user.setProfilePic(image);
                EventBus.getDefault().post(new DownloadEvents(DownloadEvents.END_DOWN_PROFILEPIC));
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break; // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
}
