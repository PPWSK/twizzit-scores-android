package com.example.vadim.twizzitscores.navdrawer;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.utilities.Helper;

import java.util.ArrayList;

public class NavDrawerAdapter extends ArrayAdapter<NavDrawerListItem> {

    private final Context context;
    private final ArrayList<NavDrawerListItem> modelsArrayList;
    private Typeface face;

    public NavDrawerAdapter(Context context,
                            ArrayList<NavDrawerListItem> modelsArrayList) {

        super(context, R.layout.drawer_list_item, modelsArrayList);

        this.context = context;
        this.modelsArrayList = modelsArrayList;
        this.face = Helper.getTypeFace(context, false);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = null;
        if (!modelsArrayList.get(position).isGroupHeader()) {
            rowView = inflater
                    .inflate(R.layout.drawer_list_item, parent, false);

            // 3. Get icon,title & counter views from the rowView
            ImageView imgView = (ImageView) rowView
                    .findViewById(R.id.icon_item);
            TextView titleView = (TextView) rowView
                    .findViewById(R.id.drawer_element_name);

            // 4. Set the text for textView
            imgView.setImageResource(modelsArrayList.get(position).getIcon());
            titleView.setTypeface(face);
            titleView.setText(modelsArrayList.get(position).getTitle());
        } else {
            // Indien header element gewenst -> deze code + xml aanmaken
            // rowView = inflater.inflate(R.layout.drawer_header_item, parent,
            //		false);
            //	TextView titleView = (TextView) rowView.findViewById(R.id.header);
            //	titleView.setText(modelsArrayList.get(position).getTitle());

        }

        return rowView;
    }

}
