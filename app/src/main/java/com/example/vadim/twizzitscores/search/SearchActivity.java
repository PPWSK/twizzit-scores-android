package com.example.vadim.twizzitscores.search;

import android.animation.TimeInterpolator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.MaterialMenuView;
import com.example.vadim.twizzitscores.HomeActivity;
import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.login.LoginActivity;
import com.example.vadim.twizzitscores.utilities.DownloadEvents;

import java.util.ArrayList;
import java.util.HashMap;

import de.greenrobot.event.EventBus;

public class SearchActivity extends FragmentActivity {

    public final static String INTENT_TEAMID = "tag frag";
    private static final int RESULTS_TOP_PADDING = 8; //see activity_search
    private final static String TAG_TEAM_SEARCH_MAP = "tag team search map";
    private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    //private static final TimeInterpolator sAccelerator = new AccelerateInterpolator();
    private static final int ANIM_DURATION = 500;
    public static HashMap<String, String> teamSearchMap;
    int mLeftDelta;
    int mTopDelta;
    float mHeightScale;
    private int mOriginalOrientation;
    private LinearLayout mContainerLL;
    private LinearLayout mSearchResults;
    private EditText mSearchText;
    private ListView mSearchList;
    private SearchArrayAdapter searchAdapter;
    private ProgressBar mSearchProgress;
    private MaterialMenuView mMatMenu;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mContainerLL = (LinearLayout) this.findViewById(R.id.containerLL);
        mSearchResults = (LinearLayout) this.findViewById(R.id.searchResultsScreen);
        mSearchText = (EditText) this.findViewById(R.id.searchText2);
        mSearchList = (ListView) this.findViewById(R.id.searchList);
        mSearchProgress = (ProgressBar) this.findViewById(R.id.searchProgressBar2);
        mMatMenu = (MaterialMenuView) this.findViewById(R.id.material_menu_button2);
        mMatMenu.setState(MaterialMenuDrawable.IconState.ARROW);
        mMatMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMatMenu.animatePressedState(MaterialMenuDrawable.IconState.BURGER);
                runExitAnimation(new Runnable() {
                    public void run() {
                        // *Now* go ahead and exit the activity
                        finish();
                        overridePendingTransition(0, 0);
                    }
                });
            }
        });

        // change status bar color in Lollipop
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getResources().getColor(R.color.twizzit_blue_dark));

        // return teams map from savedinstance
        if (savedInstanceState != null && savedInstanceState.getSerializable(TAG_TEAM_SEARCH_MAP) != null) {
            Log.d("createSearch", "savedinstance != null");
            teamSearchMap = (HashMap<String, String>) savedInstanceState.getSerializable(TAG_TEAM_SEARCH_MAP);
            createSearchViews();
        }

        final int top = getIntent().getIntExtra("top", 0);
        final int left = getIntent().getIntExtra("left", 0);
        final int height = getIntent().getIntExtra("height", 0);
        mOriginalOrientation = getIntent().getIntExtra("orientation", 0);

        // don't make animations and hide keyboard if you return from other app or from darkscreen
        if (savedInstanceState == null) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

            ViewTreeObserver observer = mSearchResults.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mSearchResults.getViewTreeObserver().removeOnPreDrawListener(this);
                    int[] screenLoc = new int[2];
                    mSearchResults.getLocationOnScreen(screenLoc);
                    mLeftDelta = left - screenLoc[0];
                    mTopDelta = top - screenLoc[1] + height - (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, RESULTS_TOP_PADDING, getResources().getDisplayMetrics());
                    mHeightScale = (float) 0 / mSearchResults.getHeight();

                    runEnterAnimation();

                    return true;
                }
            });
        }
    }

    /**
     * Eventbus handling for downloading TeamMap (Search) *
     */
    public void onEventMainThread(DownloadEvents.DownloadTeamMap dtm) {
        switch (dtm.message) {
            case DownloadEvents.START_DOWN_TEAMSLIST:
                mSearchProgress.setVisibility(View.VISIBLE);
                break;
            case DownloadEvents.END_DOWN_TEAMSLIST:
                teamSearchMap = dtm.teamMap;
                createSearchViews();
                break;
            case DownloadEvents.ERROR_DOWN_TEAMSLIST:
                mSearchProgress.setVisibility(View.GONE);
                mSearchText.setText("ERROR GETTING DATA");
                Toast.makeText(this, "error downloading teams list", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void createSearchViews() {

        if (mSearchProgress != null) {
            mSearchProgress.setVisibility(View.GONE);
        }
        searchAdapter = new SearchArrayAdapter(this, new ArrayList<>(teamSearchMap.keySet()));
        mSearchList.setAdapter(searchAdapter);

        // LastSearch makes the new search activity remember what your last search terms were
        String lastSearch = LoginActivity.user.getLastSearchTerm();
        if (lastSearch != null) {
            mSearchText.setText(lastSearch);
            mSearchText.setSelection(lastSearch.length());
            searchAdapter.getFilter().filter(lastSearch);
        }

        // filter the listView with your search query and update the lastSearch
        mSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (searchAdapter != null) {
                    searchAdapter.getFilter().filter(s);
                }
                LoginActivity.user.setLastSearchTerm(mSearchText.getText().toString());
            }
        });

        // call homeActivity, add team id, remove older activities from backstack, run animation
        mSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final String team_id = teamSearchMap.get(mSearchList.getItemAtPosition(position));
                final Intent homeIntent = new Intent(SearchActivity.this, HomeActivity.class);
                homeIntent.
                        putExtra(INTENT_TEAMID, team_id).
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // hide the soft keyboard, start the intent and override standard animations
                runExitAnimation(new Runnable() {
                    public void run() {
                        // *Now* go ahead and exit the activity
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        startActivity(homeIntent);
                        overridePendingTransition(0, 0);
                    }
                });

                //FavoritesHelper.getInstance(SearchActivity.this).saveFavorite(team_id);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().registerSticky(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (teamSearchMap != null) {
            outState.putSerializable(TAG_TEAM_SEARCH_MAP, teamSearchMap);
        }
    }

    @Override
    public void onBackPressed() {
        mMatMenu.animateState(MaterialMenuDrawable.IconState.BURGER);
        runExitAnimation(new Runnable() {
            public void run() {
                // *Now* go ahead and exit the activity
                finish();
                overridePendingTransition(0, 0);
            }
        });
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public void runEnterAnimation() {
        //final long duration = (long) (ANIM_DURATION * ActivityAnimations.sAnimatorScale);
        final long duration = (long) ANIM_DURATION;

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        mSearchResults.setPivotX(0);
        mSearchResults.setPivotY(0);
        mSearchResults.setScaleY(mHeightScale);
        mSearchResults.setTranslationX(mLeftDelta);
        mSearchResults.setTranslationY(mTopDelta);

        // Animate scale and translation to go from thumbnail to full size
        mSearchResults.animate().setDuration(duration).
                scaleY(1).
                translationX(0).translationY(0).
                setInterpolator(sDecelerator).
                withEndAction(new Runnable() {
                    public void run() {
                        mSearchList.setVisibility(View.VISIBLE);
                        ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE))
                                .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                    }
                });
    }

    /**
     * The exit animation is basically a reverse of the enter animation, except that if
     * the orientation has changed we simply scale the picture back into the center of
     * the screen.
     *
     * @param endAction This action gets run after the animation completes (this is
     *                  when we actually switch activities)
     */
    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public void runExitAnimation(final Runnable endAction) {
        final long duration = (long) ANIM_DURATION;

        final boolean fadeOut;
        if (getResources().getConfiguration().orientation != mOriginalOrientation) {
            mSearchResults.setPivotX(mContainerLL.getWidth() / 2);
            mSearchResults.setPivotY(mContainerLL.getHeight() / 2);
            mLeftDelta = 0;
            mTopDelta = 0;
            fadeOut = true;
        } else {
            fadeOut = false;
        }
        mSearchList.setVisibility(View.GONE);
        mSearchResults.animate().setDuration(duration).
                scaleY(mHeightScale).
                translationX(mLeftDelta).translationY(mTopDelta).
                withEndAction(endAction);
        if (fadeOut) {
            mSearchResults.animate().alpha(0);
        }
    }
}
