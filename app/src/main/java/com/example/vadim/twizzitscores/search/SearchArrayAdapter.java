package com.example.vadim.twizzitscores.search;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.vadim.twizzitscores.R;

import java.util.ArrayList;
import java.util.List;

public class SearchArrayAdapter extends ArrayAdapter<String> implements Filterable {

    private List<String> originalData;
    private List<String> filteredData;

    public SearchArrayAdapter(Context context, List<String> stringList) {
        super(context, 0, stringList);
        originalData = stringList;
        filteredData = stringList;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public String getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_list_item, parent, false);
        }

        String teamName = filteredData.get(position);
        CharSequence[] teamNameItems = teamName.split(":");

        TextView tv1 = (TextView) convertView.findViewById(R.id.team_name);
        tv1.setText(teamNameItems[0].toString().trim());
        TextView tv2 = (TextView) convertView.findViewById(R.id.team_name2);
        tv2.setText(teamNameItems[2].toString().trim() + " - " + teamNameItems[1].toString().trim());

        return convertView;

    }

    @Override
    public Filter getFilter() {

        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();

                //If there's nothing to filter on, return the original data for your list
                if (charSequence == null || charSequence.length() == 0) {
                    results.values = originalData;
                    results.count = originalData.size();
                } else {

                    String chars = charSequence.toString().toLowerCase();
                    CharSequence[] wordds = chars.split(" ");

                    List<String> filterResultsData = new ArrayList<>();

                    for (String data : originalData) {

                        if (wordds.length == 1) {
                            if (data.toLowerCase().contains(wordds[0])) {
                                filterResultsData.add(data);
                            }
                        } else if (wordds.length == 2) {
                            if (data.toLowerCase().contains(wordds[0]) && data.toLowerCase().contains(wordds[1])) {
                                filterResultsData.add(data);
                            }
                        } else { // 3 or more
                            if (data.toLowerCase().contains(wordds[0]) && data.toLowerCase().contains(wordds[1]) && data.toLowerCase().contains(wordds[2])) {
                                filterResultsData.add(data);
                            }
                        }
                    }

                    results.values = filterResultsData;
                    results.count = filterResultsData.size();

                }
                Log.d("GET_FILTER", "number of filtered search results: " + results.count);
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
