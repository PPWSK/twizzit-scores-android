package com.example.vadim.twizzitscores.search;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import de.greenrobot.event.EventBus;

public class SearchService extends IntentService {
    private String jsonResult;
    private OkHttpClient okClient;

    public SearchService() {
        super(null);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        DownloadEvents.DownloadTeamMap dtm = new DownloadEvents.DownloadTeamMap();
        dtm.message = DownloadEvents.START_DOWN_TEAMSLIST;
        EventBus.getDefault().postSticky(dtm);

        okClient = new OkHttpClient();
        URL url;
        try {
            url = new URL(intent.getStringExtra("uri"));
            Log.d("handleintent", "url: " + url);
            Request req = new Request.Builder()
                    .url(url)
                    .build();

            Response resp = okClient.newCall(req).execute();
            jsonResult = resp.body().string();

            dtm.teamMap = fillTeamsList();
            if (dtm.teamMap != null) {
                dtm.message = DownloadEvents.END_DOWN_TEAMSLIST;
                EventBus.getDefault().postSticky(dtm);
            } else {
                dtm.message = DownloadEvents.ERROR_DOWN_TEAMSLIST;
                EventBus.getDefault().postSticky(dtm);
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, String> fillTeamsList() throws JSONException {
        HashMap<String, String> teams_map = new HashMap<>();

        try {
            JSONObject jsonResponse = new JSONObject(jsonResult);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("result");

            // todo: check if response is positive!!

            if (jsonMainNode != null) {
                for (int i = 0; i < jsonMainNode.length(); i++) {
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    String team_name = jsonChildNode.optString("name");
                    String team_id = jsonChildNode.optString("id");
                    teams_map.put(team_name, team_id);
                }
            } else {
                teams_map = null;
            }
        } catch (JSONException e) {
            Log.d("error: LDTRY: ", e.toString());
        }
        return teams_map;
    }
}
