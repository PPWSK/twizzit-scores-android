package com.example.vadim.twizzitscores.teampages;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.favteams.FavoritesHelper;
import com.example.vadim.twizzitscores.utilities.Helper;
import com.example.vadim.twizzitscores.utilities.TeamDataCache;
import com.example.vadim.twizzitscores.utilities.TeamService;
import com.software.shell.fab.ActionButton;

public class FragmentTeam extends Fragment {

    private static final String SIS_FRAMEEXPANDED = "result_frame_expanded";
    public static String thisTeamId;
    public static boolean frameResultExpanded;
    public static boolean isAnimationBlocked = false;
    private static FragmentTeam instance;
    private static PagerAdapter viewPagerAdapter;
    private PagerSlidingTabStrip tabs;
    private ViewPager viewPager;
    private Boolean isStarPressed;
    private ActionButton aBtnLike;
    private ActionButton aBtnUp;
    private ActionButton aBtnDown;

    public static FragmentTeam newInstance(String teamId) {
        FragmentTeam f = new FragmentTeam();
        Bundle args = new Bundle();
        args.putString("teamId", teamId);
        f.setArguments(args);
        return f;
    }

    public static FragmentTeam getInstance(Context context, String teamId) {
        if (instance == null) {
            instance = FragmentTeam.newInstance(teamId);
        } else {
            if (!thisTeamId.equals(teamId)) {
                instance = FragmentTeam.newInstance(teamId);
            }
        }
        return instance;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SIS_FRAMEEXPANDED, frameResultExpanded);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thisTeamId = getArguments().getString("teamId");
        if (TeamDataCache.getInstance().getLru().get(thisTeamId) == null) {
            Intent teamIntent = new Intent(getActivity(), TeamService.class);
            teamIntent.putExtra("team_id", thisTeamId);
            getActivity().startService(teamIntent);
        }
        frameResultExpanded = savedInstanceState != null && savedInstanceState.getBoolean(SIS_FRAMEEXPANDED);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_team, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        //actionbutton
        aBtnLike = (ActionButton) rootView.findViewById(R.id.ab_like);
        aBtnDown = (ActionButton) rootView.findViewById(R.id.ab_down);
        aBtnDown.setImageResource(R.drawable.arrowdown);
        aBtnDown.setVisibility(View.INVISIBLE);
        aBtnUp = (ActionButton) rootView.findViewById(R.id.ab_up);
        aBtnUp.setImageResource(R.drawable.arrowup);
        aBtnUp.setVisibility(View.INVISIBLE);
        aBtnLike.setType(ActionButton.Type.MINI);
//        aBtnLike.setButtonColor(getResources().getColor(R.color.fab_material_cyan_500));
//        aBtnLike.setButtonColorPressed(getResources().getColor(R.color.fab_material_deep_orange_500));

        if (FavoritesHelper.getInstance(getActivity()).isFavorite(thisTeamId)) {
            aBtnLike.setState(ActionButton.State.PRESSED);
            aBtnLike.setImageResource(R.drawable.starred);
            isStarPressed = true;
        } else {
            aBtnLike.setState(ActionButton.State.NORMAL);
            aBtnLike.setImageResource(R.drawable.unstarred);
            isStarPressed = false;
        }
        aBtnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStarPressed) {
                    isStarPressed = false;
                    aBtnLike.setState(ActionButton.State.NORMAL);
                    aBtnLike.setImageResource(R.drawable.unstarred);
                    FavoritesHelper.getInstance(getActivity()).removeFavorite(thisTeamId);
                } else {
                    if (FavoritesHelper.pref_team_list.size() < 9) {
                        FavoritesHelper.getInstance(getActivity()).saveFavorite(thisTeamId);
                        aBtnLike.setState(ActionButton.State.PRESSED);
                        aBtnLike.setImageResource(R.drawable.starred);
                        isStarPressed = true;
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.max_fav_warning), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabs.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    isAnimationBlocked = false;
                } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    isAnimationBlocked = true;
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    aBtnLike.hide();
                    aBtnDown.setVisibility(View.VISIBLE);
                    aBtnDown.playShowAnimation();
                    aBtnUp.setVisibility(View.VISIBLE);
                    aBtnUp.playShowAnimation();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            aBtnDown.hide();
                            aBtnUp.hide();
                        }
                    }, 3000);
                } else {
                    aBtnLike.show();
                    if (!aBtnDown.isHidden()) aBtnDown.hide();
                    if (!aBtnUp.isHidden()) aBtnUp.hide();
                }

            }
        });
        // set tabs lines colors
        int col = Helper.getColorFromTeamID(thisTeamId);
        tabs.setIndicatorColor(col);
        tabs.setUnderlineColor(col);
        tabs.setShouldExpand(true);
        // until here
        // create ripple effect
        int[] attrs = new int[]{R.attr.selectableItemBackgroundBorderless};
        TypedArray typedArray = getActivity().obtainStyledAttributes(attrs);
        int backgroundResource = typedArray.getResourceId(0, 0);
        tabs.setTabBackground(backgroundResource);
        // until here
        tabs.setViewPager(viewPager);

        return rootView;
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final String[] TITLES = getActivity().getResources().getStringArray(R.array.tab_titles);
        int PAGE_COUNT = TITLES.length;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        // THIS IS MAGIC! --> doesn't retain instances of fragments
        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag = null;
            Bundle args = new Bundle();
            if (position == 0) {
                frag = new FragmentTeamGames();
                args.putString(FragmentTeamGames.ARG_OBJECT, thisTeamId);
            } else if (position == 1) {
                frag = new FragmentTeamDivision();
                args.putString(FragmentTeamDivision.ARG_OBJECT, thisTeamId);
            } else if (position == 2) {
                frag = new FragmentTeamRanking();
                args.putString(FragmentTeamRanking.ARG_OBJECT, thisTeamId);
            } else {
                Log.d("fragment_getitem", "error");
            }
            if (frag != null) {
                frag.setArguments(args);
            }
            return frag;
        }
    }

}



