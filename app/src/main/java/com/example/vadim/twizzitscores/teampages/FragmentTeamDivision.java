package com.example.vadim.twizzitscores.teampages;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.example.vadim.twizzitscores.utilities.Helper;
import com.example.vadim.twizzitscores.utilities.MoveAnimation;
import com.example.vadim.twizzitscores.utilities.TeamDataCache;
import com.software.shell.fab.ActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import de.greenrobot.event.EventBus;

public class FragmentTeamDivision extends Fragment {
    public static final String ARG_OBJECT = "team_id";
    private final SimpleDateFormat DFORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    private HashMap<String, LinkedHashMap<Integer, ArrayList<String>>> eventMapList;
    private String week;
    private String year;
    private FrameLayout mFrameLive;
    private String teamId;
    private FrameLayout mFrameResult;
    private ActionButton aBtnUp;
    private ActionButton aBtnDown;
    private ListView divListview;
    private TextView mListDivTitle;
    private int frameLiveHeight;
    private int frameResultHeight;
    private float frameResultY;
    private float mPreviousY;
    private Runnable runnerHideButtons;
    private Handler handlerHideButtons;
    private LinearLayout.LayoutParams resultParams;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFrameLive = (FrameLayout) getActivity().findViewById(R.id.frameLive);
        mFrameResult = (FrameLayout) getActivity().findViewById(R.id.frameResult);
        getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getView().getViewTreeObserver().removeGlobalOnLayoutListener(this);

                frameLiveHeight = mFrameLive.getHeight();
                frameResultHeight = mFrameResult.getHeight();
                frameResultY = mFrameResult.getY();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_team_division, container, false);

        teamId = getArguments().get(ARG_OBJECT).toString();
        rootView.setBackgroundColor(Helper.getColorFromTeamID(teamId));

        aBtnDown = (ActionButton) getActivity().findViewById(R.id.ab_down);
        aBtnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putRightDataInListView(nextWeek());
                callButtonsAgain();
            }
        });
        aBtnUp = (ActionButton) getActivity().findViewById(R.id.ab_up);
        aBtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putRightDataInListView(previousWeek());
                callButtonsAgain();
            }
        });
        divListview = (ListView) rootView.findViewById(R.id.list_div_games);
        divListview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                final int PADDING = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
                ViewConfiguration vc = ViewConfiguration.get(v.getContext());
                final int mTouchSlop = vc.getScaledTouchSlop();
                float y = event.getY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        callButtonsAgain();
                        break;
                    case MotionEvent.ACTION_MOVE:

                        //event.getRawY();
                        float dY = y - mPreviousY;
                        Log.d("ontouch", "dy, y, previousY :" + dY + ", " + y + ", " + mPreviousY);

                        if (dY < 0 && !FragmentTeam.isAnimationBlocked) {
                            Log.d("dd", "swiping up : " + y);
                            if (!FragmentTeam.frameResultExpanded) {
                                MoveAnimation heightAnim = new MoveAnimation(mFrameResult, (int) mFrameResult.getY(), (int) mFrameResult.getY() - frameLiveHeight);
                                heightAnim.setDuration(500);
                                heightAnim.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                        resultParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (frameResultHeight + frameLiveHeight + PADDING));
                                        mFrameResult.setLayoutParams(resultParams);
                                        FragmentTeam.isAnimationBlocked = true;
                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        FragmentTeam.isAnimationBlocked = false;
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                v.startAnimation(heightAnim);
                                FragmentTeam.frameResultExpanded = true;
                                mPreviousY = y;
                                return true;

                            }
                        } else if (dY > 0 && Math.abs(dY) > mTouchSlop && !FragmentTeam.isAnimationBlocked) {
                            Log.d("dd", "swiping down : " + y);
                            if (FragmentTeam.frameResultExpanded && Helper.listIsAtTop(divListview)) {

                                MoveAnimation heightAnim = new MoveAnimation(mFrameResult, (int) frameResultY - frameLiveHeight, (int) frameResultY);
                                heightAnim.setDuration(500);
                                heightAnim.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                        FragmentTeam.isAnimationBlocked = true;
                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        resultParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (frameResultHeight));
                                        mFrameResult.setLayoutParams(resultParams);
                                        FragmentTeam.isAnimationBlocked = false;
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                v.startAnimation(heightAnim);

                                FragmentTeam.frameResultExpanded = false;
                                mPreviousY = y;
                                return true;
                            } else {
                                return false;
                            }
                        }

                        break;
                    default:
                        break;
                }

                mPreviousY = y;
                return false;
            }
        });

        mListDivTitle = (TextView) rootView.findViewById(R.id.list_div_title);
        mListDivTitle.setTypeface(Helper.getTypeFace(getActivity(), false));
        if (TeamDataCache.getInstance().getLru().get(teamId) != null) {
            setDivisionList(teamId);
        }
        return rootView;
    }

    public void onEventMainThread(DownloadEvents se) {
        switch (se.message) {
            case DownloadEvents.START_DOWN_TEAMDATA:
                //TODO: here: show loader or cancel gestures
                break;
            case DownloadEvents.END_DOWN_TEAMDATA:
                Log.d("FragmentTeamDivision", "FragmentTeamDivision: END_DOWN_TEAMDATA; args: " + teamId);
                setDivisionList(teamId);
                break;
            case DownloadEvents.ERROR_DOWN_TEAMDATA:
                Toast.makeText(getActivity(), "error downloading team data", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void setDivisionList(String team_id) {
        eventMapList = new HashMap<>();

        try {
            JSONObject jsonResponse = (JSONObject) TeamDataCache.getInstance().getLru().get(team_id);
            //todo: check for jsonResponse == null, happens sometimes
            if (jsonResponse.optJSONObject("result").optJSONObject("division") == null) {
                errorLoadingData("result.division == null");
            } else {
                // divisionWeekJson is a JsonArray that contains 2014_36, 2014_37, ..., 2015_14
                JSONArray divisionWeekJson = jsonResponse.optJSONObject("result").optJSONObject("division").names();
                if (divisionWeekJson != null) {
                    for (int i = 0; i < divisionWeekJson.length(); i++) {
                        LinkedHashMap<Integer, ArrayList<String>> eventMap = new LinkedHashMap<>();

                        // divisionEventJson is a JsonArray that contains all events (approx 6) that take place in a certain week: 2014_36 for example.
                        JSONArray divisionEventJson = jsonResponse.optJSONObject("result").optJSONObject("division").optJSONArray(divisionWeekJson.getString(i));

                        if (divisionEventJson != null) {
                            // for the first week of the season
                            for (int j = 0; j < divisionEventJson.length(); j++) {
                                JSONObject eventJO = divisionEventJson.getJSONObject(j);

                                String dateToParse = eventJO.getString("F_START_DATE").trim() + " " + eventJO.getString("F_START_TIME").trim();
                                Date eventDate = DFORMAT.parse(dateToParse);
                                ArrayList event = new ArrayList<>();

                                event.add(eventJO.getString("F_EVENT_TITLE"));
                                event.add(eventJO.getString("F_START_DATE"));
                                event.add(eventJO.getString("F_START_TIME"));
                                event.add(eventJO.getString("F_SCORE"));
                                eventMap.put(i, event);
                            }
                        } else {
                            // for all following weeks
                            // THIS IS DUE TO VERY BAD JSON: first result is an array, while others are objects...
                            divisionEventJson = jsonResponse.optJSONObject("result").optJSONObject("division").optJSONObject(divisionWeekJson.getString(i)).names();
                            for (int k = 0; k < divisionEventJson.length(); k++) {
                                JSONObject eventJO = jsonResponse.optJSONObject("result").optJSONObject("division").optJSONObject(divisionWeekJson.getString(i)).optJSONObject(divisionEventJson.getString(k));

                                String dateToParse = eventJO.getString("F_START_DATE").trim() + " " + eventJO.getString("F_START_TIME").trim();
                                Date eventDate = DFORMAT.parse(dateToParse);
                                ArrayList event = new ArrayList<>();

                                event.add(eventJO.getString("F_EVENT_TITLE"));
                                event.add(eventJO.getString("F_START_DATE"));
                                event.add(eventJO.getString("F_START_TIME"));
                                event.add(eventJO.getString("F_SCORE"));
                                eventMap.put(divisionEventJson.getInt(k), event); //(Integer) divisionEventJson.getInt(k)
                            }
                        }
                        // eventMapList ( key: i, value : TreeMap ( Key : Date, value : ArrayList of event data ) )
                        eventMapList.put(divisionWeekJson.get(i).toString(), eventMap);
                    }
                } else {
                    errorLoadingData("division week json != null");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        putRightDataInListView("none");
    }

    private void callButtonsAgain() {
        if (aBtnUp.isHidden() && aBtnDown.isHidden()) {
            aBtnDown.show();
            aBtnUp.show();
        } else if (!aBtnUp.isHidden() && !aBtnDown.isHidden()) {
            if (handlerHideButtons != null && runnerHideButtons != null) {
                handlerHideButtons.removeCallbacks(runnerHideButtons);
            }
        }
        handlerHideButtons = new Handler();
        runnerHideButtons = new Runnable() {
            @Override
            public void run() {
                aBtnDown.hide();
                aBtnUp.hide();
            }
        };
        handlerHideButtons.postDelayed(runnerHideButtons, 3000);
    }

    private String nextWeek() {
        int intWeek = Integer.valueOf(week);
        int intYear = Integer.valueOf(year);
        if (intWeek == 52) {
            intWeek = 1;
            intYear++;
        } else {
            intWeek++;
        }
        week = String.valueOf(intWeek);
        year = String.valueOf(intYear);
        return year + "_" + week;
    }

    private String previousWeek() {
        int intWeek = Integer.valueOf(week);
        int intYear = Integer.valueOf(year);
        if (intWeek == 1) {
            intWeek = 52;
            intYear--;
        } else {
            intWeek--;
        }
        week = String.valueOf(intWeek);
        year = String.valueOf(intYear);
        return year + "_" + week;
    }

    private void putRightDataInListView(String rightWeek) {
        divListview.setAdapter(null);
        if (rightWeek.equals("none")) {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            year = String.valueOf(today.year);
            week = String.valueOf(today.getWeekNumber());
            String thisWeek = year + "_" + week;
            mListDivTitle.setText(thisWeek.replace("_", ", week: "));

            if (eventMapList.get(thisWeek) != null) {
                divListview.setAdapter(new ResultArrayAdapter(getActivity(), new ArrayList<>(eventMapList.get(thisWeek).values())));

            }
        } else {
            mListDivTitle.setText(rightWeek.replace("_", ", week: "));
            if (eventMapList.get(rightWeek) != null) {
                divListview.setAdapter(new ResultArrayAdapter(getActivity(), new ArrayList<>(eventMapList.get(rightWeek).values())));
            }
        }
    }

    private void errorLoadingData(String msg) {
        //Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}