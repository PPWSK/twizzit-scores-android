package com.example.vadim.twizzitscores.teampages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.utilities.DownloadEvents;
import com.example.vadim.twizzitscores.utilities.Helper;
import com.example.vadim.twizzitscores.utilities.MoveAnimation;
import com.example.vadim.twizzitscores.utilities.TeamDataCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import de.greenrobot.event.EventBus;

public class FragmentTeamGames extends Fragment {
    public static final String ARG_OBJECT = "team_id";
    private final SimpleDateFormat DFORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    private ListView gamesListview;
    private TextView mListTitle;
    private String teamId;
    private FrameLayout mFrameLive;
    private FrameLayout mFrameResult;
    private int frameLiveHeight;
    private int frameResultHeight;
    private float frameResultY;
    private float mPreviousY;
    private LinearLayout.LayoutParams resultParams;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFrameLive = (FrameLayout) getActivity().findViewById(R.id.frameLive);
        mFrameResult = (FrameLayout) getActivity().findViewById(R.id.frameResult);
        getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getView().getViewTreeObserver().removeGlobalOnLayoutListener(this);

                frameLiveHeight = mFrameLive.getHeight();
                frameResultHeight = mFrameResult.getHeight();
                frameResultY = mFrameResult.getY();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_team_games, container, false);

        teamId = getArguments().get(ARG_OBJECT).toString();
        rootView.setBackgroundColor(Helper.getColorFromTeamID(teamId));

        gamesListview = (ListView) rootView.findViewById(R.id.list_team_games);
        gamesListview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                final int PADDING = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
                ViewConfiguration vc = ViewConfiguration.get(v.getContext());
                final int mTouchSlop = vc.getScaledTouchSlop();
                float y = event.getY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_MOVE:

                        //event.getRawY();
                        float dY = y - mPreviousY;
                        Log.d("ontouch", "dy, y, previousY :" + dY + ", " + y + ", " + mPreviousY);

                        if (dY < 0 && !FragmentTeam.isAnimationBlocked) {
                            Log.d("dd", "swiping up : " + y);
                            if (!FragmentTeam.frameResultExpanded) {
                                MoveAnimation heightAnim = new MoveAnimation(mFrameResult, (int) mFrameResult.getY(), (int) mFrameResult.getY() - frameLiveHeight);
                                heightAnim.setDuration(500);
                                heightAnim.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                        resultParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (frameResultHeight + frameLiveHeight + PADDING));
                                        mFrameResult.setLayoutParams(resultParams);
                                        FragmentTeam.isAnimationBlocked = true;
                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        FragmentTeam.isAnimationBlocked = false;
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                v.startAnimation(heightAnim);
                                FragmentTeam.frameResultExpanded = true;
                                mPreviousY = y;
                                return true;

                            }
                        } else if (dY > 0 && Math.abs(dY) > mTouchSlop && !FragmentTeam.isAnimationBlocked) {
                            Log.d("dd", "swiping down : " + y);
                            if (FragmentTeam.frameResultExpanded && Helper.listIsAtTop(gamesListview)) {

                                MoveAnimation heightAnim = new MoveAnimation(mFrameResult, (int) frameResultY - frameLiveHeight, (int) frameResultY);
                                heightAnim.setDuration(500);
                                heightAnim.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                        FragmentTeam.isAnimationBlocked = true;
                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        resultParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (frameResultHeight));
                                        mFrameResult.setLayoutParams(resultParams);
                                        FragmentTeam.isAnimationBlocked = false;
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                v.startAnimation(heightAnim);

                                FragmentTeam.frameResultExpanded = false;
                                mPreviousY = y;
                                return true;
                            } else {
                                return false;
                            }
                        }

                        break;
                    default:
                        break;
                }

                mPreviousY = y;
                return false;
            }
        });

        mListTitle = (TextView) rootView.findViewById(R.id.list_title);
        mListTitle.setTypeface(Helper.getTypeFace(getActivity(), false));
        if (TeamDataCache.getInstance().getLru().get(teamId) != null) {
            setGamesList(teamId);
        }
        return rootView;
    }

    public void onEventMainThread(DownloadEvents se) {
        switch (se.message) {
            case DownloadEvents.START_DOWN_TEAMDATA:
                //TODO: here: show loader or cancel gestures
                break;
            case DownloadEvents.END_DOWN_TEAMDATA:
                //TODO: ERROR: this doesn't mean it's the right TEAMDATA that whas done!!
                setGamesList(teamId);
                break;
            case DownloadEvents.ERROR_DOWN_TEAMDATA:
                Toast.makeText(getActivity(), "error downloading team data", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private void setGamesList(String team_id) {
        LinkedHashMap<Integer, ArrayList<String>> eventMap = new LinkedHashMap<>();
        //todo: in order to have team, at low speeds, it's better to store list in sqlIte db
        mListTitle.setText("here comes team name");
        try {
            JSONObject jsonResponse = (JSONObject) TeamDataCache.getInstance().getLru().get(team_id);
            //todo: check for jsonResponse == null!! happens sometimes, because not the right TEAMDATA THAT WAS LOADED
            if (jsonResponse.optJSONObject("result").optJSONObject("games") != null) {
                JSONArray gamesJson = jsonResponse.optJSONObject("result").optJSONObject("games").names();
                if (gamesJson != null) {

                    for (int i = 0; i < gamesJson.length(); i++) {
                        String eventId = gamesJson.getString(i);
                        JSONObject eventJO = jsonResponse.optJSONObject("result").optJSONObject("games").getJSONObject(eventId);

                        // parse date so treeMap can order automatically
                        String dateToParse = eventJO.getString("F_START_DATE").trim() + " " + eventJO.getString("F_START_TIME").trim();
                        Date eventDate = DFORMAT.parse(dateToParse);
                        ArrayList event = new ArrayList<>();
                        event.add(eventJO.getString("F_EVENT_TITLE"));
                        event.add(eventJO.getString("F_START_DATE"));
                        event.add(eventJO.getString("F_START_TIME"));
                        event.add(eventJO.getString("F_SCORE"));
                        eventMap.put(i, event);
                    }
                }
            } else {
                ArrayList<String> event = new ArrayList<>();
                event.add(null);
                eventMap.put(0, event);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        gamesListview.setAdapter(new ResultArrayAdapter(getActivity(), new ArrayList<>(eventMap.values())));
    }
}
