package com.example.vadim.twizzitscores.teampages;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.utilities.Helper;

public class FragmentTeamRanking extends Fragment {
    public static final String ARG_OBJECT = "object";
    private String teamId;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_team_ranking, container, false);
        Bundle args = getArguments();
        teamId = args.getString(ARG_OBJECT);
        rootView.setBackgroundColor(Helper.getColorFromTeamID(teamId));
        return rootView;
    }
}
