package com.example.vadim.twizzitscores.teampages;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.vadim.twizzitscores.R;
import com.example.vadim.twizzitscores.utilities.Helper;

import java.util.ArrayList;
import java.util.List;

public class ResultArrayAdapter extends ArrayAdapter<ArrayList<String>> {

    TextView mResultDate;
    TextView mResultTeams;
    TextView mResultScore;
    private List<ArrayList<String>> mData;
    private Typeface face;

    public ResultArrayAdapter(Context context, List<ArrayList<String>> data) {
        super(context, 0, data);
        this.face = Helper.getTypeFace(context, false);
        this.mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public ArrayList<String> getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.result_list_item, parent, false);
        }

        ArrayList<String> resultList = mData.get(position);
        mResultDate = (TextView) convertView.findViewById(R.id.result_date);
        mResultDate.setTypeface(face);
        mResultTeams = (TextView) convertView.findViewById(R.id.result_teams);
        mResultTeams.setTypeface(face);
        mResultScore = (TextView) convertView.findViewById(R.id.result_score);
        mResultScore.setTypeface(face);

        if (resultList.get(0) != null) {
            mResultDate.setText(resultList.get(1).toString() + System.getProperty("line.separator") + resultList.get(2).toString());
            mResultTeams.setText(resultList.get(0));
            mResultScore.setText(resultList.get(3));
        } else {
            mResultTeams.setText("No data for this team");
            mResultTeams.setAllCaps(true);
        }

        return convertView;
    }
}
