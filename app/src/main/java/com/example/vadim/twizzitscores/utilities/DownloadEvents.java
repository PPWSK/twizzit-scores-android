package com.example.vadim.twizzitscores.utilities;

import java.util.HashMap;

public class DownloadEvents {
    //TODO: since we're putting everything in seperate event classes, we can change to START, END, ERROR
    public static final int START_DOWN_TEAMSLIST = 10;
    public static final int END_DOWN_TEAMSLIST = 11;
    public static final int ERROR_DOWN_TEAMSLIST = 12;
    public static final int START_DOWN_PROFILEPIC = 20;
    public static final int END_DOWN_PROFILEPIC = 21;
    public static final int ERROR_DOWN_PROFILEPIC = 22;
    public static final int START_DOWN_TEAMDATA = 30;
    public static final int END_DOWN_TEAMDATA = 31;
    public static final int ERROR_DOWN_TEAMDATA = 32;
    public static final int SHAREDPREFS_CHANGED = 41;
    public final int message;

    public DownloadEvents(int message) {
        this.message = message;
    }

    public static class DownloadTeamMap {
        public HashMap<String, String> teamMap;
        public int message;
    }
}
