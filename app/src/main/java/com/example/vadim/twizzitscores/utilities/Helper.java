package com.example.vadim.twizzitscores.utilities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.ListView;

import java.util.Map;

public class Helper {

    public static final String[] colowrs = {"#9C27B0", "#00BCD4", "#CDDC39", "#FF5722", "#607D8B", "#3F51B5", "#F44336", "#795548", "#FFC107"};
    // 0 = purple, 1 = cyan, 2 = lime, 3 = deep orange, 4 = blue grey, 5 = indigo, 6 = red , 7 = brown, 8 = amber

    //todo: add lighter versions of colowrs, for viewpager background

    public static final String[] lightColowrs = {"#3F51B5", "#2196F3", "#03A9F4", "#00BCD4", "#009688", "#00796B", "#7C4DFF", "#E040FB", "#9C27B0"};
    // 0 = indigo, 1 = blue, 2 = light blue, 3 = cyan, 4 = teal, 5 = darkteal, 6 = deeppurpleaccent, 7 = purpleaccent, 8 = purpleprimary

    public static int getColorFromTeamID(String teamId) {
        return Color.parseColor(colowrs[Integer.valueOf(teamId.substring(teamId.length() - 1)) - 1]);
    }

//    public static final View.OnTouchListener getListener(Context context, FrameLayout bottomFrame, FrameLayout topFrame){
//
//        final Context cont = context;
//        final FrameLayout mFrameResult = bottomFrame;
//        final FrameLayout mFrameLive = topFrame;
//        final LinearLayout.LayoutParams resultParams;
//
//
//        View.OnTouchListener listener = new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                final int PADDING = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, cont.getResources().getDisplayMetrics());
//                float y = event.getY();
//                float mPreviousY = y;
//                boolean isAnimationBlocked = false;
//
//                switch (event.getAction() & MotionEvent.ACTION_MASK) {
//                    case MotionEvent.ACTION_MOVE:
//
//                        //event.getRawY();
//                        float dY = y - mPreviousY;
//                        Log.d("ontouch", "dy, y, previousY :" + dY + ", " + y + ", " + mPreviousY);
//
//                        if (dY < 0 && !isAnimationBlocked) {
//                            Log.d("dd", "swiping up : " + y);
//                            if (!FragmentTeam.frameResultExpanded) {
//                                MoveAnimation heightAnim = new MoveAnimation(mFrameResult, (int) mFrameResult.getY(), (int) mFrameResult.getY() - frameLiveHeight);
//                                heightAnim.setDuration(500);
//                                heightAnim.setAnimationListener(new Animation.AnimationListener() {
//                                    @Override
//                                    public void onAnimationStart(Animation animation) {
//                                        resultParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (frameResultHeight + frameLiveHeight + PADDING));
//                                        mFrameResult.setLayoutParams(resultParams);
//                                        isAnimationBlocked = true;
//                                    }
//
//                                    @Override
//                                    public void onAnimationEnd(Animation animation) {
//                                        isAnimationBlocked = false;
//                                    }
//
//                                    @Override
//                                    public void onAnimationRepeat(Animation animation) {
//
//                                    }
//                                });
//                                v.startAnimation(heightAnim);
//                                FragmentTeam.frameResultExpanded = true;
//                                mPreviousY = y;
//                                return true;
//
//                            }
//                        } else if (dY > 0 && !isAnimationBlocked) {
//                            Log.d("dd", "swiping down : " + y);
//                            if (FragmentTeam.frameResultExpanded && Helper.listIsAtTop(divListview)) {
//
//                                MoveAnimation heightAnim = new MoveAnimation(mFrameResult, (int) frameResultY - frameLiveHeight, (int) frameResultY);
//                                heightAnim.setDuration(500);
//                                heightAnim.setAnimationListener(new Animation.AnimationListener() {
//                                    @Override
//                                    public void onAnimationStart(Animation animation) {
//                                        isAnimationBlocked = true;
//                                    }
//
//                                    @Override
//                                    public void onAnimationEnd(Animation animation) {
//                                        resultParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (frameResultHeight));
//                                        mFrameResult.setLayoutParams(resultParams);
//                                        isAnimationBlocked = false;
//                                    }
//
//                                    @Override
//                                    public void onAnimationRepeat(Animation animation) {
//
//                                    }
//                                });
//                                v.startAnimation(heightAnim);
//
//                                FragmentTeam.frameResultExpanded = false;
//                                mPreviousY = y;
//                                return true;
//                            } else {
//                                return false;
//                            }
//                        }
//
//                        break;
//                    default:
//                        break;
//                }
//
//                mPreviousY = y;
//                return false;
//            }
//        };
//        return listener;
//    }

    public static Object getKeyFromValue(Map hm, Object value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o;
            }
        }
        return null;
    }

    public static Typeface getTypeFace(Context context, boolean bold) {
        if (bold) {
            return Typeface.createFromAsset(context.getAssets(), "fonts/Rubrik-Bold.ttf");
        } else {
            return Typeface.createFromAsset(context.getAssets(), "fonts/Rubrik-Regular.ttf");
        }
    }

    public static boolean listIsAtTop(ListView listView) {
        if (listView.getChildCount() == 0) return true;
        return listView.getChildAt(0).getTop() == 0;
    }

    //todo: add team map here? so no need to pass it around

    //todo: add user here? so no need to pass it around
}
