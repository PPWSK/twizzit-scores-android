package com.example.vadim.twizzitscores.utilities;


import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class MoveAnimation extends Animation {
    protected final int originalY;
    protected final View view;
    protected float perValue2;

    public MoveAnimation(View view, int fromY, int toY) {
        this.view = view;
        this.originalY = fromY;
        this.perValue2 = (toY - fromY);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        view.setY(originalY + perValue2 * interpolatedTime);
        //view.requestLayout();
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
