package com.example.vadim.twizzitscores.utilities;

import android.support.v4.util.LruCache;

public class TeamDataCache {
    private static TeamDataCache instance;
    private LruCache<Object, Object> lru;

    //TODO: REPLACE WITH OKHTTP CACHE

    private TeamDataCache() {

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        lru = new LruCache<>(cacheSize);
    }

    public static TeamDataCache getInstance() {

        if (instance == null) {

            instance = new TeamDataCache();
        }

        return instance;

    }

    public LruCache<Object, Object> getLru() {
        return lru;
    }
}


