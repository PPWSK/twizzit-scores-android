package com.example.vadim.twizzitscores.utilities;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;

import de.greenrobot.event.EventBus;

public class TeamService extends IntentService {

    private final static String BASE_URI = "http://develop.twizzit.com/webservice.php?";

    private String jsonResult;
    private JSONObject jsonResponse;
    private OkHttpClient okClient;

    public TeamService() {
        super(null);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String team_id = intent.getStringExtra("team_id");
        EventBus.getDefault().post(new DownloadEvents(DownloadEvents.START_DOWN_TEAMDATA));

        if (TeamDataCache.getInstance().getLru().get(team_id) == null) {
            okClient = new OkHttpClient();

            Uri builtUri = Uri.parse(BASE_URI).buildUpon()
                    .appendQueryParameter("page", "rankingsandresults")
                    .appendQueryParameter("key", "amo85g3e46z")
                    .appendQueryParameter("lg", Integer.toString(2))
                    .appendQueryParameter("action", "get_rr_data")
                    .appendQueryParameter("teamid", team_id)
                    .build();

            Log.d("teamservice", builtUri.toString());

            try {
                Request req = new Request.Builder()
                        .url(builtUri.toString())
                        .build();

                Response resp = okClient.newCall(req).execute();
                jsonResult = resp.body().string();
                //TODO: check if not 404 before next line
                jsonResponse = new JSONObject(jsonResult);

                TeamDataCache.getInstance().getLru().put(team_id, jsonResponse);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        EventBus.getDefault().post(new DownloadEvents(DownloadEvents.END_DOWN_TEAMDATA));

    }
}
